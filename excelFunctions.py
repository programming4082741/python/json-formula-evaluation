# -*- coding: utf-8 -*-
"""
# Excel formulas: Python Implementation

@author: Yassir Abbassi
"""

# Excel Functions
def SUM(*args):
    try:
        return sum(args)
    except TypeError:
        return '#Error: type does not match'


def MULTIPLY(*args):
    result = 1
    try:
        for value in args:
            result *= value
        return result
    except TypeError:
        return '#Error: type does not match'


def DIVIDE(a, b):
    try:
        if b != 0:
            return a/b
        else:
            return '#Error: ?'
    except TypeError:
        return '#Error: type does not match'


def GT(a, b):
    try:
        return a > b
    except TypeError:
        return '#Error: type does not match'


def LT(a, b):
    try:
        return a < b
    except TypeError:
        return '#Error: type does not match'


def EQ(a, b):
    try:
        return a == b
    except TypeError:
        return '#Error: type does not match'


def NOT(arg):
    if arg == 'True':
        arg = True
    elif arg == 'False':
        arg = False
    else:
        return '#Error: type does not match'
    return not arg


def AND(*args):
    processed_args = []
    for arg in args:
        # Convert string 'True' or 'False' to boolean
        if isinstance(arg, str):
            if arg == 'True':
                processed_args.append(True)
            elif arg == 'False':
                processed_args.append(False)
            else:
                return '#Error: type does not match'
        else:
            return '#Error: type does not match'
    return all(processed_args)
    

def OR(*args):
    processed_args = []
    for arg in args:
        # Convert string 'True' or 'False' to boolean
        if isinstance(arg, str):
            if arg == 'True':
                processed_args.append(True)
            elif arg == 'False':
                processed_args.append(False)
            else:
                return '#Error: type does not match'
        else:
            return '#Error: type does not match'
    return any(processed_args)


def IF(condition, true_value, false_value):
    try:
        if not isinstance(condition, bool):
            return '#Error: type does not match'
        return true_value if condition else false_value
    except TypeError:
        return '#Error: type does not match'


def CONCAT(*args):
    try:
        return ''.join(map(str, args))
    except TypeError:
        return '#Error: type does not match'
    