# -*- coding: utf-8 -*-
"""
# Utility functions

@author: Yassir Abbassi
"""

# Import dependencies
import re


# Convert A1 notation to 0-index
def a1_0Index(a1_notation):
    # Extract column letters from A1 notation using regex
    column_letters = re.search(r"([A-Z]+)", a1_notation).group()

    # Convert column letters to column index
    column = 0
    for i, char in enumerate(reversed(column_letters)):
        column += (ord(char) - ord("A") + 1) * (26 ** i)

    # Extract row number from A1 notation using regex
    row = int(re.search(r"(\d+)$", a1_notation).group()) - 1  # Convert to 0-index

    return [row, column-1]


# Returns whether the string is a cell notation or not
def isCell(input_string):
    # Regular expression pattern to match Excel cell references
    pattern = r'\b[A-Z]+[0-9]+\b'
    
    matches = re.findall(pattern, input_string)
    return bool(matches)


# Returns list dimensions
def listDim(lst):
    if isinstance(lst, list) and lst != []:
        return (len(lst)-1, len(lst[0])-1)
    else:
        return (-1, -1)


# Find Python function name
def find_keywords_in_string(input_string):
    keywords = ['SUM', 'MULTIPLY', 'DIVIDE', 'GT', 'EQ', 'NOT', 'AND', 'OR',
                'IF', 'CONCAT']
    found_keywords = []
    for keyword in keywords:
        if keyword.lower() in input_string.lower():
            found_keywords.append(keyword)
    return found_keywords


# Extract Excel cell references
def extract_cell_references(formula):
    # Regular expression pattern to match cell references
    pattern = r'([A-Z]+\d+)'
    
    cell_references = list(set(re.findall(pattern, formula)))
    return cell_references


# To replace formula and arguments with their respective values
def replace_words_in_string(input_string, replacement_dict):
    for key, value in replacement_dict.items():
        # Add quotes around non-numeric values
        if not value.isnumeric():
            value = f'"{value}"'
        input_string = input_string.replace(key, value)
    return input_string

def replace_words_in_string2(input_string, replacement_dict):
    for key, value in replacement_dict.items():
        input_string = input_string.replace(key, value)
    return input_string

