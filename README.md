# Data Transformation and API Interaction Project

This repository is focused on retrieving data from an API, performing data transformations, and interacting with custom functions. Below, you'll find an overview of the project and snippets of code used in this endeavor.

## Project Overview

### Project Description

This project involves several key tasks, including making API requests, processing the obtained data, evaluating formulas, and interacting with custom functions. Below are the primary components of this project:

### Making API Requests

```python
# API endpoint
url_get = "https://"

# GET request
response = requests.get(url_get)
dataIn = response.json()
```

We start by sending a GET request to a specified API endpoint and store the retrieved data in `dataIn`.

### Data Transformation

We transform the data by evaluating equal formulas (e.g., "=B2") and custom functions in each data sheet. These transformations involve extracting cell references, resolving dependencies, and updating cell values.

### Generating POST Request Data

```python
dataOut = {}
dataOut['email'] = 'local@domain.com'
dataOut['results'] = dataIn['sheets']
```

We generate data to be sent in a POST request, including email information and the processed data.

### API Interaction and POST Request

```python
# POST Request
url_post = dataIn['submissionUrl']
post_response = requests.post(url_post, json=dataOut)
```

We send a POST request with the processed data to the specified URL.

### Response

```python
# Response & Passcode
post_response_json = post_response.json()
print(post_response_json)
code = post_response_json
```

We retrieve the response from the POST request and obtain a passcode.

## Project Goals

- Retrieve data from an external API.
- Process data by evaluating formulas and custom functions.
- Interact with the API by sending a POST request with the transformed data.
- Retrieve and display the response, including the passcode.