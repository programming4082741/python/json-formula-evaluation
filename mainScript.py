# -*- coding: utf-8 -*-
"""

@author: Yassir Abbassi
"""

import requests

from utilityFunctions import *
from excelFunctions import *


# API endpoint
url_get = "https://"


# GET request
response = requests.get(url_get)
dataIn = response.json()


# Generating POST request data

dataOut = {}
dataOut['email'] = 'local@domain.com'
dataOut['results'] = dataIn['sheets']


# Evaluate all equal formulas (for instance: =B2)
for sheet in dataIn['sheets']:
    lst = sheet['data']
    n,m = listDim(lst)
    
    if (n,m) == (-1,-1):
        continue
    
    # Loop through each data sheet
    args = {}
    for i in range(n+1):
        for j in range(m+1):
            
            # Check for the simple equal formula
            if isinstance(lst[i][j], str) and lst[i][j].startswith('='):
                formula = lst[i][j]
                if find_keywords_in_string(formula) == []:
                    cells = extract_cell_references(formula)[0]
                
                    x, y = a1_0Index(cells)
                    args[cells] = str(lst[x][y])
                    
                    cList = []
                    
                    if isCell(args[cells]) == True:
                        
                        cList.append([i, j])
                        cList.append([x, y])
                        a, b = x, y
                        
                        # Loop until all cells are resolved
                        while cList != []:
                            if(extract_cell_references(lst[a][b]) != []):
                                cells = extract_cell_references(lst[a][b])[0]
                                a, b = a1_0Index(cells)
                                args[cells] = str(lst[a][b])
                                cList.append([a, b])
                            else:
                                for l in range(len(cList)):
                                    k = cList.pop()
                                    lst[k[0]][k[1]] = lst[a][b]
                    else:
                        lst[i][j] = lst[x][y]
                    
                    
# Evaluate all custom functions ()
for sheet in dataIn['sheets']:
    lst = sheet['data']
    n,m = listDim(lst)
    
    if (n,m) == (-1,-1):
        continue
    
    # Loop through each data sheet
    args = {}
    for i in range(n+1):
        for j in range(m+1):
            
            # Check for the remaining formula cells with custom functions
            if isinstance(lst[i][j], str) and lst[i][j].startswith('='):
                formula = lst[i][j]
                cells = extract_cell_references(formula)
                
                for cell in cells:
                    if cell in args:
                        continue
                    x, y = a1_0Index(cell)
                    args[cell] = str(lst[x][y])
                
                # Generate & Execute the formula
                if find_keywords_in_string(formula) != []:
                    formula = replace_words_in_string(formula[1:], args)
                    resVar = 'temp'
                    exec(f"{resVar} = {formula}")
                    lst[i][j] = temp



# POST Request
url_post = dataIn['submissionUrl']
post_response = requests.post(url_post, json = dataOut)


# Response & Passcode
post_response_json = post_response.json()
print(post_response_json)
code = post_response_json
